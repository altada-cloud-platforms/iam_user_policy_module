resource "aws_iam_user" "user" {
  name = var.name
}

resource "aws_iam_user_policy_attachment" "s3_access" {
  count = var.attach_s3_policy ? 1 : 0
  user       = aws_iam_user.user.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_user_policy_attachment" "batch_access" {
  count = var.attach_batch_policy ? 1 : 0
  user       = aws_iam_user.user.name
  policy_arn = "arn:aws:iam::aws:policy/AWSBatchFullAccess"
}

