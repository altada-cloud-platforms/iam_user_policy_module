variable "name" {
  description = "Name of the User"
  type = string
  default = ""
}

variable "attach_s3_policy" {
  description = "attach s3 policy"
  type        = bool
  default     = false
}

variable "attach_batch_policy" {
  description = "attach batch policy"
  type        = bool
  default     = false
}


